# Pasos para recrear este bucket

## Crear repo y un archivo de README.md

- `$ mkdir basic_app`
- `$ cd basic_app`
- `$ git init`
- `$ touch README.md`
- Hacer cambios en el archivo `README.md`

## Revisar los cambios y hacer el primer commit

- `$ git status`

```bash
  On branch master

  No commits yet

  Untracked files:
    (use "git add <file>..." to include in what will be committed)

          README.md

  nothing added to commit but untracked files present (use "git add" to track)
```

- `$ git add .` (también se puede ser más específico `$ git add README.md`)

- `$ git status`

```bash
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   README.md
```

- `$ git commit -m "Initial commit"`
- `$ git status`

```bash
On branch master
nothing to commit, working tree clean
```

## Crear las ramas de develop y test

El proceso que estamos usando es una variación [GitFlow](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow), entonces debemos crear las ramas de develop y test.

- `$ git branch develop && git checkout develop` o bien se puede crear la rama y hacer checkout a la vez usando `$ git checkout -b develop` **unicamente al crear la rama.**

- Repetir el proceso pero con la rama de test `$ git checkout -b test`

- Ver las nuevas ramas creadas: `$ git branch`

Estas dos ramas existirán durante todo la vida del proyecto y son vitales para el proceso de `DevOps` y `CI/CD`.

## Trabajar en la primer tarea

Asumiendo que las tareas están creadas siguiendo metodologías ágiles y se usa un board (como Jira), entonces tenemos un número de ticket asignado a la tarea y se asume que es el ticket `AWSAM-1`. Entonces se procede de la siguiente forma:

- Posicionarse en la rama base, que en nuestro casos es _develop_ `$ git checkout develop`
- Crear la rama local de trabajo: `$ git checkout -b feature/AWSAM-1`
- Crear la carpeta de trabajo `$ mkdir src`, aquí se podría crear toda la infraestructura base del proyecto, como archivos de configuración, etc.
- Crear archivo el archivo de la aplicación: `$ touch src/app.py`
- Hacer los cambios a `src/app.py`.
- Se recomienda hacer cambios pequeños e incrementales, que sumados solucionen la tarea en el ticket:

  1. `$ git add src/app.py`
  1. `$ git commit -m "feat: Add first couple of functions"`
  1. Repetir estos pasos, hasta que la tarea este lista para pasar a la etapa de revisión, en nuestro caso uno o dos commits con fines didácticos.

- Cuando la solución esté lista para se probada por calidad (QA o testing), entonces se procede a fundir (merge) el código con develop, por el momento ya que estamos trabajando unicamente local solo se debe hacer el paso marcado con ✓, los otros pasos son unicamente como referencia para el futuro.

  1. [Ignorar este paso si se trabaja unicamente en local] Es muy importante verificar que mis cambios estén al día con la rama base `$ git checkout develop` y después `$ git fetch` y finalmente `$ git pull`
  1. [Ignorar este paso si se trabaja unicamente en local] Aplicar cualquier cambio en mi rama de trabajo `$ git checkout feature/AWSAM-1`, después `$ git rebase origin/develop`
  1. ✓ **Ahora sí se puede proceder a hacer el merge** `$ git checkout develop` y finalmente `$ git merge --no-ff feature/AWSAM-1`.
  1. [Ignorar este paso si se trabaja unicamente en local] Solucionar cualquier posible conflicto (tema para la segunda etapa de esta charla)
  1. [Ignorar este paso si se trabaja unicamente en local] Finalmente se comparte la nueva rama de develop con el mundo: `$ git push`

## Pasos extra

En caso de terminar esto rápido, se pueden hacer los siguientes pasos para poner a prueba los aprendido:

- Hacer merge de la rama de `develop` hacia `test`
- Hacer merge de la rama de `test` hacia `master`
- Volver a la rama de `develop` y crear una nueva rama llamada `feature/AWSAM-2` y agregar algún cambio a `src/app.py`, para finalmente hacer el proceso de merge a `develop`
